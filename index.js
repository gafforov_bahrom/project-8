let time = "00:00";
let x = 0; 
let isWorking = false;
let a = [];
let timeList = '';

function start() {
    x = parseInt(localStorage.getItem("time"), 10);
    if (isNaN(x)) {
        x = 0;
    }

    isWorking = true;
    
}

function add (){
    if (a.length>0){timeList = ''; }
    a.push(time);
    for (let i = 0; i<a.length;i++){
        timeList += '<div>' +'<div>'+ a[a.length-1-i]+'</div>'+"<button onclick='deleteTime("+parseInt(a.length-1-i)+")'>"+"x"+'</button>' + '</div>'; 
    }
    document.getElementById('list').innerHTML = timeList;
}

function deleteTime(i){
    console.log(i);
    a.splice(i,1);
    timeList = '';
    for (let i = 0; i<a.length;i++){
        if(a.length>0){
            timeList += '<div>' +'<div>'+ a[a.length-1-i]+'</div>'+"<button onclick='deleteTime("+parseInt(a.length-1-i)+")'>"+"x"+'</button>' + '</div>';
        } else timeList = ''
         
    }
    document.getElementById('list').innerHTML = timeList;
    // console.log(timeList);
}

function stop() {
    isWorking = false;
}

function reset() {
    isWorking = false;
    time = "00:00";
    x = 0;
    a=[];
    timeList = [];
    for (let i = 0; i<a.length;i++){
        timeList += '<div>' + a[a.length-1-i] + '</div>'; 
    }
    document.getElementById('list').innerHTML = timeList;

    localStorage.setItem("time", x);
    document.getElementById("watch").innerHTML = time;
}

function toString(x) {
    const seconds = x%60;
    const minutes = Math.floor(x / 60);
    if (minutes < 10) {
        time = "0"+minutes;
    } else {
        time = ""+minutes;
    }
    time += ":"
    if (seconds < 10) {
        time += "0"+seconds;
    } else {
        time += ""+seconds;
    }
    return time
}

function updateTimer() {
    x = parseInt(localStorage.getItem("time"), 10);
    if (isNaN(x)) {
        x = 0;
    }
    if (isWorking) {
        x++;
    }

    if(isWorking){
        document.getElementById('start').style.display = 'none';
        document.getElementById('pause').style.display = 'inline-block';
    } else {document.getElementById('pause').style.display = 'none';
    document.getElementById('start').style.display = 'inline-block';}

    time = toString(x);
    document.getElementById("watch").innerHTML = time;
    localStorage.setItem("time", x);

    
}

updateTimer();
setInterval(updateTimer, 1000);